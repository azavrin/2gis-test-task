const gulp = require('gulp');
const babelify = require('babelify');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const plugins = require('gulp-load-plugins')();
const less = require('gulp-less');
const path = require('path');

gulp.task('less', function () {
  return gulp.src('./app/css/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less') ]
    }))
    .pipe(gulp.dest('./app/css/'));
});

gulp.task('lint', function () {
    return gulp.src(['app/**/*.js'])
        .pipe(plugins.eslint())
        .pipe(plugins.eslint.format())
        .pipe(plugins.eslint.failAfterError())
        .pipe(plugins.jscs());
});

gulp.task('browserify', function () {
    return browserify({
        entries: './build.js',
        debug: true,
        transform: [babelify]
    })
        .bundle()
        .pipe(source('lib-build.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('compile', function () {
    return gulp.src('dist/lib-build.js')
        .pipe(plugins.uglify())
        .pipe(plugins.rename({ extname: '.min.js' }))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', plugins.sequence('less', 'browserify', 'compile', 'lint'));
