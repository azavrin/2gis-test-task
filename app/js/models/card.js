import * as Backbone from "backbone";
"use strict";

class CardModel extends Backbone.Model {
    get defaults() {
        return {
            type: "" //narrow or wide
        };
    }
}

export default CardModel;
