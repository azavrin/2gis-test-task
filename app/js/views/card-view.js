import * as Backbone from "backbone";
import $ from "jquery";
import * as _ from "underscore";
"use strict";

class CardView extends Backbone.View {
	get template() {return _.template($("#item-template").html()); }

    initialize() {
        this.listenTo(this.model, "destroy", function () { this.remove(); });
    }

    render() {
        const html = this.template(this.model.toJSON());
        this.$el.html(html);
        this.setElement(html);
        return this;
    }
}

export default CardView;
