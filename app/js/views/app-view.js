import * as Backbone from "backbone";
import CardView from "./card-view.js";
import CardModel from "../models/card.js";
import $ from "jquery";
"use strict";

class AppView extends Backbone.View {
    get el() { return $("#app"); }

    get events() {
        return {
            "click #card-list": function (e) { this.clickHandler(e); }
        };
    }

    initialize() {
        this.addAll();
        this.listenTo(this.collection, "add", function (card) { this.addOne(card); });
        const $body = $(document.body);
        function setBackgroundColor(color) {
            $body.css({ "background-color": color });
        }

        this.$("#card-list").on({
            mouseenter: function () { setBackgroundColor("#f6f2de"); },
            mouseleave: function () { setBackgroundColor("#e9e6d3"); }
        });

        $body.on({
            mouseenter: function () { setBackgroundColor("#e9e6d3"); },
            mouseleave: function () { setBackgroundColor("#f6f2de"); }
        });
    }

    render() {
        this.collection.fetch({ reset: true });
    }

    changeWidth() {
        if (this.collection.length === 1) {
            this.$("#card-list").css({ "min-width": "400px" }).width(this.collection.models[0].get("type") === "narrow" ? 400 : "100%");
        } else {
            this.$("#card-list").css({ "min-width": "460px" }).width(this.collection.models[this.collection.length - 1].get("type") === "narrow" ? 460 : "100%");
        }
    }

    addOne(card) {
        const view = new CardView({ model: card });
        this.$("#card-list").append(view.render().el);
        this.changeWidth();
    }

    addAll() {
        this.collection.each(this.addOne, this);
    }

    createCard(type) {
        const model = new CardModel({
            type: type,
            number: this.collection.length + 1
        });
        this.collection.add(model);
    }

    removeCard() {
        if (this.collection.length > 1) {
            const model = this.collection.pop();
            if (model) {
                model.destroy();
            }
        }
        this.changeWidth();
    }

    clickHandler(e) {
        if (e.shiftKey && e.altKey) {
            this.createCard("wide");
        } else if (e.shiftKey) {
            this.createCard("narrow");
        } else {
            this.removeCard();
        }
    }
}

export default AppView;
