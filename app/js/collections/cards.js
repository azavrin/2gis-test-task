import * as Backbone from "backbone";
import Card from "../models/card.js";
"use strict";

class CardsCollection extends Backbone.Collection {
	get model() { return Card; }
}

export default CardsCollection;
