# Test task for 2gis

### Installation
```bash
$ git clone https://azavrin@bitbucket.org/azavrin/2gis-test-task.git
$ cd 2gis-test-task
$ npm start
```
### Available gulp tasks
* `gulp lint` - runs eslint and jscs
* `gulp less` - compile css from less files
* `gulp browserify` - builds the script for browser
* `gulp compile` - runs uglify and generates minified script
* `gulp` - default task, runs lint, browserify and compile tasks
