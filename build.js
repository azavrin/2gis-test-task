import * as Backbone from 'backbone';
import CardsCollection from "./app/js/collections/cards";
import AppView from './app/js/views/app-view.js';
"use strict";

((cards_object) => {
	for (let i = 0; i < cards_object.length; i++){
		cards_object[i].number = i+1;
	}
    const collection = new CardsCollection(cards_object);
    const appView = new AppView({
	    collection: collection
	});
	Backbone.history.start();
})(cards);
